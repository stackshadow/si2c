// This is the configuration file for i2c
// it will be include inside the ASM

// you need to setup SDA and SDL PIN / PORT to use this library correctly
#define     confi2cSCLPort D
#define     confi2cSCLPin 3
#define     confi2cSDAPort D
#define     confi2cSDAPin 4

#define     confi2cAddress 0x04
// SCL -> PD3 -> (braun) -> GPIO23
// SDA -> PD4 -> (rot) -> GPIO24

// comment this define out if you dont have an status LED
#define     confi2c_statusLED_Port D
#define     confi2c_statusLED_Pin 7


